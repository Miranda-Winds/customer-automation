package com.customer.pages;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.customer.base.TestBase;
import com.customer.util.ExcelUtil;

import io.appium.java_client.MobileElement;

public class LoginPage extends TestBase{

	

	/*@FindBy(id="com.template:id/edt_email")
	WebElement un;
	@FindBy(id="com.template:id/edt_password")
	WebElement pwd;
	@FindBy(name="Sign in")
	WebElement sign_btn;  */
	 
	
    public LoginPage() {
	
    	
	}
    
    public void login(String sheetName,int uname_row,int uname_col,int pwd_row,int pwd_col) throws Exception
    {
    	
    	MobileElement un=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/edt_email\")");
        MobileElement pwd=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/edt_password\")");
        MobileElement signin_btn=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/btn_signup\")");
      
        
        String sUserName = ExcelUtil.setExcelFileSheet(sheetName,uname_row, uname_col);
        
        String sPassword = ExcelUtil.setExcelFileSheet(sheetName,pwd_row, pwd_col);
    
        un.clear();
 
        un.sendKeys(sUserName);
        pwd.clear();
      
    	pwd.sendKeys(sPassword);
    	signin_btn.click();
    }
    
  /*  public void login(String uname,String pswd)
    {
    	
    	MobileElement un=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/edt_email\")");
        MobileElement pwd=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/edt_password\")");
        MobileElement signin_btn=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/btn_signup\")");
    	un.sendKeys(uname);
    	pwd.sendKeys(pswd);
    	signin_btn.click();
    }*/
    
    public void signUp()
    {
    	 MobileElement signup_btn=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/tv_register\")");
    	signup_btn.click();
    }
	
	public void rememberMe()
	{
		  MobileElement remember_checkbox=(MobileElement)ad.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.winds.customer:id/radiobutton_remberme\")");
		remember_checkbox.click();
	}
}
