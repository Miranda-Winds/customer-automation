package com.customer.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.pattern.PropertiesPatternConverter;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Platform;

import com.customer.base.TestBase;

//import utility.Constant;

public class ExcelUtil extends TestBase{
	
	

	
	   
	//Main Directory of the project
	    public static final String currentDir = System.getProperty("user.dir");
	 
	    //Location of Test data excel file
	    public static String testDataExcelPath = null;
	 
	    //Excel WorkBook
	    private static XSSFWorkbook excelWBook;
	 
	    //Excel Sheet
	    private static XSSFSheet excelWSheet;
	 
	    //Excel cell
	    private static XSSFCell cell;
	 
	    //Excel row
	    private static XSSFRow row;
	 
	    //Row Number
	    public static int rowNumber;
	 
	    //Column Number
	    public static int columnNumber;
	 
	    //Test Sheet name
	    
	    
	 
	    // This method has two parameters: "Test data excel file name" and "Excel sheet name"
	    // It creates FileInputStream and set excel file and excel sheet to excelWBook and excelWSheet variables.
	    public static String setExcelFileSheet(String sheetName,int RowNum,int ColNum) {
	  String path= System.getProperty("user.dir");
	          
	  testDataExcelPath = currentDir+"/src/main/java/com/customer/testData/TestData.xlsx";
	        try {
	            // Open the Excel file
	    
	            FileInputStream ExcelFile = new FileInputStream(testDataExcelPath );//+ prop.getProperty("testDataExcelFileName"));
	            excelWBook = new XSSFWorkbook(ExcelFile);
	            excelWSheet = excelWBook.getSheet(sheetName);
	            // Read and format value from sheet
	            DataFormatter formatter = new DataFormatter();
	            String cell = formatter.formatCellValue(excelWSheet.getRow(RowNum).getCell(ColNum));
	            System.out.println("cell data:"+cell);
	             return cell;
	          } catch (Exception e) {
	            try {
	                throw (e);
	            } catch (IOException e1) {
	                e1.printStackTrace();
	            }
	            return "Exception";
	        }
	    }
	 
	  
	    
	    /*public static String getCellData(int RowNum, int ColNum) throws Exception{
	    	 
	        try{
	  
	            cell = excelWSheet.getRow(RowNum).getCell(ColNum);
	  
	            String CellData = cell.getStringCellValue();
	            System.out.println("cell data:"+CellData);
	  
	            return CellData;
	  
	            }catch (Exception e){
	  
	  return "Exception";
	  
	            }
	  
	      }
	    
	     */
	 
	    //This method takes row number as a parameter and returns the data of given row number.
	    public static XSSFRow getRowData(int RowNum) {
	        try {
	        	
	        	System.out.println("Row num in getRowData()"+RowNum);
	        	System.out.println("excelsheet"+excelWSheet);
	            row = excelWSheet.getRow(RowNum);
	           
	            System.out.println(row);
	            return row;
	        } catch (Exception e) {
	            throw (e);
	        }
	    }
	 
	    //This method gets excel file, row and column number and set a value to the that cell.
	    public static void setCellData(String value, int RowNum, int ColNum) {
	        try {
	           
	            // Constant variables Test Data path and Test Data file name
	            FileOutputStream fileOut = new FileOutputStream(testDataExcelPath + prop.getProperty("testDataExcelFileName"));
	            excelWBook.write(fileOut);
	            fileOut.flush();
	            fileOut.close();
	        } catch (Exception e) {
	            try {
	                throw (e);
	            } catch (IOException e1) {
	                e1.printStackTrace();
	            }
	        }
	    }
	    
	   
	}
	
